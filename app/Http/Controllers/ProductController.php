<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view ('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('category_name', 'id')->prepend('<--Select Category-->','');
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $rules = [

            "product_name" => 'required',
            "category_id"  => 'required',
            "image"        => 'required'
        ];
        $this->validate($request, $rules);
        DB::beginTransaction();
        try {
            $data = $request->except(['_token']);
            //$data['user_id']=auth()->user()->id;
            $image = $request->file('image');
            $file_name = $data['product_name'].".".$image->getClientOriginalExtension();
            // Storage::disk('public')->put('/',$image);
            $image->move(storage_path('app/public'),$file_name);
            $data['image_path'] = $file_name;
            unset($data['image']);
            //dd($data);
            Product::insert($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('products.index')->with('error_message', $e->getMessage());
        }
        return redirect()->route('products.index')->with('success_message', "Product added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::with('Category')->where('id', $id)->first();

        return view('products.show', compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::pluck('category_name', 'id')->prepend('<--Select Category-->','');
        $product = Product::find($id);
        return view('products.edit', compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      
        $rules = [
            "product_name" => 'required',
            "category_id"  => 'required',
            "image"        => 'required'
        ];
        $this->validate($request,$rules);
        DB::beginTransaction();
        try{
            $product = Product::findOrFail($id);
            $data = $request->except(['_token', '_method']);
            //dd($data);
            $product->update($data);
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->route('products.index')->with('error_message',$e->getMessage());

        }
        return redirect()->route('products.index')->with('success_message',"Product updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
