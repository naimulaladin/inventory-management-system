<?php

namespace App\Http\Controllers;
use App\Http\Requests\SellValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Sell;
use App\Models\Inventory;
use App\Models\Product;
use App\Models\User;

class SellsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sells = Sell::with('sells')->get();
        //dd($sells);
        // return $sells;
        
        return view('sells.index',compact('sells'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::pluck('product_name','id')->prepend('<--select a product-->', '');
        return view('sells.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SellValidation $request)
    {
       $rules = [
            "product_id"=>'required',
            "sell_date"=>'required',
            "quantity"=>'required',
            "price_original"=>'required',
            "price_selling"=>'required',
        ];
        $this->validate($request,$rules);
        DB::beginTransaction();
        try{
            $sell = Sell::findOrFail($id);
            $vendor->update($request->all());
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->route('sells.index')->with('error_message',$e->getMessage());

        }
        return redirect()->route('sells.index')->with('success_message',"Product price updated successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
