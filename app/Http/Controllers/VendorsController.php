<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Vendor;

class VendorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::all();
        return view('vendors.index',compact('vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->all());
        $rules = [
            "company_name"       => 'required',
            "ceo_name"           => 'required',
            "address"            => 'required',
            "contact_person_name"=>'required',
            "mobile"             =>'required|unique:vendors',
            "email"              =>'required|unique:vendors',

        ];
        $this->validate($request,$rules);
        //dd($request->all());
        DB::beginTransaction();
        try{
            Vendor::create($request->all());
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->route('vendors.index')->with('error_message',$e->getMessage());

        }
        return redirect()->route('vendors.index')->with('success_message',"Vendor created successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vendor = Vendor::find($id);
        //dd($vendor);
        return view('vendors.show',compact('vendor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor = Vendor::find($id);
        return view('vendors.edit',compact('vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            "company_name"=>'required',
            "ceo_name"=>'required',
            "address"=>'required',
            "contact_person_name"=>'required',
            "mobile"=>'required|unique:vendors,mobile,'.$id,
            "email"=>'required|unique:vendors,email,'.$id,

        ];
        $this->validate($request,$rules);
        DB::beginTransaction();
        try{
            $vendor = Vendor::findOrFail($id);
            $vendor->update($request->all());
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->route('vendors.index')->with('error_message',$e->getMessage());

        }
        return redirect()->route('vendors.index')->with('success_message',"Vendor updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
