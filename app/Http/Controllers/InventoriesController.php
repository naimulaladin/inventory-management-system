<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Inventory;
use App\Models\Vendor;
use Illuminate\Support\Facades\Storage;

class InventoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inven = Inventory::with('vendor')->get();
      //  dd($inven);
        return view('inventory.index',compact('inven'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendors = Vendor::pluck('company_name','id');
        $vendors->prepend('--Select a vendor--','');
        // return $vendors;
        return view('inventory.create',compact('vendors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "vendor_id"     => 'required',
            "product_name"  => 'required',
            "purchase_date" => 'required',
            "product_des"   => 'required',
            "price"         => 'required',
            'image'=>'required'
        ];
        // return $request->all();
        $this->validate($request, $rules);
        DB::beginTransaction();
        try {
            $data = $request->all();
            //$data['user_id']=auth()->user()->id;
            $image = $request->file('image');
            $file_name = $data['product_name'].".".$image->getClientOriginalExtension();
            // Storage::disk('public')->put('/',$image);
            $image->move(storage_path('app/public'),$file_name);
            $data['image_path'] = $file_name;
            unset($data['image']);
            Inventory::create($data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('inventories.index')->with('error_message', $e->getMessage());
        }
        return redirect()->route('inventories.index')->with('success_message', "Inventory created successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
