<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
   protected $table = 'inventories';
   protected $guarded = ['id'];

   public function vendor(){
   		return $this->belongsTo(Vendor::class,'vendor_id');
   }
   public function users()
   {
   		return $this->belongsTo(User::class,'user_id');
   }
}
