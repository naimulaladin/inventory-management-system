<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    protected $table = 'sells';
    protected $guarded = ['id'];

    public function products(){
      return $this->belongsTo(product::class,'product_id');
    }
    public function users()
   {
   		return $this->belongsTo(User::class,'user_id');
   }
}
