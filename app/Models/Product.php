<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = 'id';

    public function Category()
    {
    	return $this->belongsTo(Category::class, 'category_id');
    }
}
