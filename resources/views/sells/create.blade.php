@extends('layouts.app')
@section('content')
<div class="container-fluid">
	{!! Form::open(['route'=>'sells.store'])!!}
	<div class="card" style="overflow: hidden">
		<div class="card-header">
			<strong>Sells Entry</strong>
			<small>Form</small>
		</div>
		<div class="card-body card-block">
			<div class="form-group">
				{!! Form::label('product_id','Product',['class'=>'form-control-label']) !!}
				<!-- <label for="company" class=" form-control-label">Company Name</label> -->
				{!! Form::select("product_id",$products,null,['class'=>"form-control"])!!}
				<!-- <input type="text" id="company" placeholder="Enter your company name" class="form-control"> -->
				{!! $errors->first('product_id','<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="form-group">
				{!! Form::label('sell_date', 'Purchase date', ['class'=>'form-control-label']) !!}
				<!-- <label for="ceo_name" class=" form-control-label">CEO Name</label> -->
				{!! Form::date('sell_date', null, ['placeholder'=>'Enter sell date', 'class'=>'form-control']) !!}
				<!-- <input type="text" id="vat" placeholder="Enter your CEO name" class="form-control"> -->
				{!! $errors->first('purchase_date', '<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="form-group">
					{!! Form::label('quantity', 'Quantity:', ['class'=>'form-control-label']) !!}
						<!-- <label for="postal-code" class=" form-control-label">Mobile NO: </label> -->
					{!! Form::number('quantity', null, ['placeholder'=>'Enter amount', 'class'=>'form-control']) !!}
						<!-- <input type="text" id="postal-code" placeholder="Enter your mobile number" class="form-control"> -->
					{!! $errors->first('quantity', '<p class="text text-danger">:message</p>') !!}
					</div>
			<div class="form-group">
				{!! Form::label('price_original', 'Price(Original)', ['class'=>'form-control-label']) !!}
				<!-- <label for="ceo_name" class=" form-control-label">CEO Name</label> -->
				{!! Form::number('price_original', null, ['placeholder'=>'Enter Original Price', 'class'=>'form-control']) !!}
				<!-- <input type="text" id="vat" placeholder="Enter your CEO name" class="form-control"> -->
				{!! $errors->first('price_original', '<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="form-group">
				{!! Form::label('price_selling', 'Price(Selling)', ['class'=>'form-control-label']) !!}
				<!-- <label for="ceo_name" class=" form-control-label">CEO Name</label> -->
				{!! Form::number('price_selling', null, ['placeholder'=>'Enter Selling Price', 'class'=>'form-control']) !!}
				<!-- <input type="text" id="vat" placeholder="Enter your CEO name" class="form-control"> -->
				{!! $errors->first('price_selling', '<p class="text text-danger">:message</p>') !!}
			</div>
		</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary btn-lg pull-right">
				<i class="fa fa-dot-circle-o"></i> Submit
			</button>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@endsection