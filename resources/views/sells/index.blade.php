@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{Session::get('success_message')}}
            </div>
        @endif
        @if(Session::has('error_message'))
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{Session::get('error_message')}}
            </div>
        @endif
        <a class="btn btn-primary pull-right" style="margin-bottom: 10px;" href="{{URL::route('sells.create')}}">
            ADD NEW
            <i class="fas fa-plus-circle"></i>
        </a>
        <div class="table-responsive m-b-40">

            <table class="table table-borderless table-data3">
                <thead>
                    <tr>
                        <th>SL. No</th>
                        <th>Product</th>
                        <th>Sell Date</th>
                        <th>Quantity</th>
                        <th>User ID</th>
                    </tr>
                </thead>
                <tbody>
                    @php($i=1)
                    @forelse($sells as $sell)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$sell->products?$sell->products->product_name:'n/a'}}</td>
                            <td>{{$sell->sell_date}}</td>
                            <td>{{$sell->quantity}}</td>
                            <td>{{$sell->users->name}}</td>
                            
                        </tr>
                    @empty
                    <tr>
                        <td colspan="6" class="bg-warning">No Data available</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection()