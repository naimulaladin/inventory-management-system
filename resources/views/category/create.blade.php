@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        {!! Form::open(['route'=>'categories.store'])!!}
        <div class="card" style="overflow: hidden">
            <div class="card-header">
                <strong>Ctegory Entry</strong>
                <small>Form</small>
            </div>
            <div class="card-body card-block">
                <div class="form-group">
                {!! Form::label('category_name','Category',['class'=>'form-control-label']) !!}
                {!! Form::text("category_name",null,['placeholder'=>'Enter your category name','class'=>"form-control"])!!}
                {{--{!! $errors->first('company_name','<p class="text text-danger">:message</p>') !!}--}}
                </div>
                <div class="form-group">
                {!! Form::label('category_description','Category Description',['class'=>'form-control-label']) !!}
                {!! Form::text("category_description",null,['placeholder'=>'Enter your category description','class'=>"form-control"])!!}
                {{--{!! $errors->first('company_name','<p class="text text-danger">:message</p>') !!}--}}
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-lg pull-right">
                    <i class="fa fa-dot-circle-o"></i> Submit
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection