@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{Session::get('success_message')}}
            </div>
        @endif
        @if(Session::has('error_message'))
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{Session::get('error_message')}}
            </div>
        @endif
        <a class="btn btn-primary pull-right" style="margin-bottom: 10px;" href="{{URL::route('inventories.create')}}">
            ADD NEW
            <i class="fas fa-plus-circle"></i>
        </a>
        <div class="table-responsive m-b-40">

            <table class="table table-borderless table-data3">
                <thead>
                    <tr>
                        <th>SL. No</th>
                        <th>Product Name</th>
                        <th>Product Description</th>
                        <th>Price</th>
                        <th>Vendor</th>
                        <th>User ID</th>
                        <th>Images</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    @php($i=1)
                    @forelse($inven as $inventory)
                       
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$inventory->product_name}}</td>
                            <td>{{$inventory->product_des}}</td>
                            <td>{{$inventory->price}}</td>
                            <td>{{$inventory->vendor->company_name}}</td>
                            <td>{{$inventory->users->name}}</td>
                            <td><img style="width: 100px;" class="img-responsive" src="{{asset('storage/'.$inventory->image_path)}}"></td>
                            <td>{{$inventory->quantity}}</td>
                        </tr>
                      

                    @empty
                    <tr>
                        <td colspan="8" class="bg-warning">No Data available</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection()