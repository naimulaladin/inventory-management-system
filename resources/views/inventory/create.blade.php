@extends('layouts.app')
@section('content')
<div class="container-fluid">
	{!! Form::open(['route'=>'inventories.store','files' => true])!!}
	<div class="card" style="overflow: hidden">
		<div class="card-header">
			<strong>Inventory Entry</strong>
			<small>Form</small>
		</div>
		<div class="card-body card-block">
			<div class="form-group">
				{!! Form::label('product_name','Product Name:',['class'=>'form-control-label']) !!}
				<!-- <label for="company" class=" form-control-label">Company Name</label> -->
				{!! Form::text("product_name",null,['placeholder'=>'Enter your product name','class'=>"form-control"])!!}
				<!-- <input type="text" id="company" placeholder="Enter your company name" class="form-control"> -->
				{!! $errors->first('product_name','<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="form-group">
				{!! Form::label('vendor_id','Vendor Name:',['class'=>'form-control-label']) !!}
				<!-- <label for="company" class=" form-control-label">Company Name</label> -->
				{!! Form::select("vendor_id",$vendors,null,['class'=>"form-control"])!!}
				<!-- <input type="text" id="company" placeholder="Enter your company name" class="form-control"> -->
				{!! $errors->first('vendor_id','<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="form-group">
				{!! Form::label('purchase_date', 'Purchase date', ['class'=>'form-control-label']) !!}
				<!-- <label for="ceo_name" class=" form-control-label">CEO Name</label> -->
				{!! Form::date('purchase_date', null, ['class'=>'form-control']) !!}
				<!-- <input type="text" id="vat" placeholder="Enter your CEO name" class="form-control"> -->
				{!! $errors->first('purchase_date', '<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="form-group">
				{!! Form::label('price_per_product', 'Price per product', ['class'=>'form-control-label']) !!}
				<!-- <label for="ceo_name" class=" form-control-label">CEO Name</label> -->
				{!! Form::number('price_per_product', null, ['placeholder'=>'Enter the price', 'class'=>'form-control']) !!}
				<!-- <input type="text" id="vat" placeholder="Enter your CEO name" class="form-control"> -->
				{!! $errors->first('price_per_product', '<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="row form-group">
				<div class="col-8">
					<div class="form-group">
						{!! Form::label('quantity', 'Quantity:', ['class'=>'form-control-label']) !!}
						<!-- <label for="postal-code" class=" form-control-label">Mobile NO: </label> -->
						{!! Form::number('quantity', null, ['placeholder'=>'Enter amount', 'class'=>'form-control']) !!}
						<!-- <input type="text" id="postal-code" placeholder="Enter your mobile number" class="form-control"> -->
						{!! $errors->first('quantity', '<p class="text text-danger">:message</p>') !!}
					</div>
				</div>
				<div class="col-8">
					<div class="form-group">
						{!! Form::label('total_amount', 'Total Amount', ['class'=>'form-control-label']) !!}
						<!-- <label for="city" class=" form-control-label">Contact Person</label> -->
						{!! Form::number('total_amount', null, ['placeholder'=>'Enter price', 'class'=>'form-control']) !!}
						<!-- <input type="text" id="city" placeholder="Enter contact person name" class="form-control"> -->
						{!! $errors->first('total_amount', '<p class="text text-danger">:message</p>') !!}
					</div>
				</div>
				<div class="col-8">
					{!!Form::file('image')!!}
				</div>
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary btn-lg pull-right">
				<i class="fa fa-dot-circle-o"></i> Submit
			</button>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@endsection