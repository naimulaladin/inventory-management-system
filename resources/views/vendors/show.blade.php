@extends('layouts.app')
@section('content')
<div class="col-lg-6">
    <!-- TOP CAMPAIGN-->
    <div class="top-campaign">
        <h3 class="title-3 m-b-30">Detail Information</h3>
        <div class="table-responsive">
            <table class="table table-top-campaign">
                <tbody>
                  @if(isset($vendor))
                    <tr>
                        <td>Company Name:</td>
                        <td class="float-left">{{$vendor->company_name}}</td>
                    </tr>
                     <tr>
                        <td>CEO's Name:</td>
                        <td class="float-left">{{$vendor->ceo_name}}</td>
                    </tr>
                    <tr>
                        <td>Contact Person Name:</td>
                        <td  class="float-left">{{$vendor->contact_person_name}}</td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td class="float-left">{{$vendor->email}}</td>
                    </tr>
                    <tr>
                        <td>Mobile NO:</td>
                        <td class="float-left">{{$vendor->mobile}}</td>
                    </tr>
                     <tr>
                        <td>Address:</td>
                        <td class="float-left">{{$vendor->mobile}}</td>
                    </tr>
                     @else
                        <tr>
                            <td colspan="4" class="bg-warning">No Data available</td>
                        </tr>
                     @endif
                   
                </tbody>
            </table>
        </div>
    </div>
    <!--  END TOP CAMPAIGN-->
</div>
@endsection