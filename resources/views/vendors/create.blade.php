@extends('layouts.app')
@section('content')
<div class="container-fluid">
	{!! Form::open(['route'=>'vendors.store'])!!}
	<div class="card" style="overflow: hidden">
		<div class="card-header">
			<strong>Vedor Entry</strong>
			<small> Form</small>
		</div>
		<div class="card-body card-block">
			<div class="form-group">
				{!! Form::label('company_name','Company Name ',['class'=>'form-control-label']) !!}
				<!-- <label for="company" class=" form-control-label">Company Name</label> -->
				{!! Form::text("company_name",null,['placeholder'=>'Enter your company name','class'=>"form-control"])!!}
				<!-- <input type="text" id="company" placeholder="Enter your company name" class="form-control"> -->
				{!! $errors->first('company_name','<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="form-group">
				{!! Form::label('ceo_name', 'CEO Name', ['class'=>'form-control-label']) !!}
				<!-- <label for="ceo_name" class=" form-control-label">CEO Name</label> -->
				{!! Form::text('ceo_name', null, ['placeholder'=>'Enter your CEO name', 'class'=>'form-control']) !!}
				<!-- <input type="text" id="vat" placeholder="Enter your CEO name" class="form-control"> -->
				{!! $errors->first('ceo_name', '<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="form-group">
				{!! Form::label('address', 'Address', ['class'=>'form-control-label']) !!}
				<!-- <label for="street" class=" form-control-label">Address</label> -->
				{!! Form::text('address', null, ['placeholder'=>'Enter your address', 'class'=>'form-control']) !!}
				<!-- <input type="text" id="street" placeholder="Enter your address" class="form-control"> 
				-->
				{!! $errors->first('address', '<p class="text text-danger">:message</p>') !!}
			</div>
			<div class="row form-group">
				<div class="col-8">
					<div class="form-group">
						{!! Form::label('contact_person_name', 'Contact Person', ['class'=>'form-control-label']) !!}
						<!-- <label for="city" class=" form-control-label">Contact Person</label> -->
						{!! Form::text('contact_person_name', null, ['placeholder'=>'Enter contact person name', 'class'=>'form-control']) !!}
						<!-- <input type="text" id="city" placeholder="Enter contact person name" class="form-control"> -->
						{!! $errors->first('contact_person_name', '<p class="text text-danger">:message</p>') !!}
					</div>
				</div>
				<div class="col-8">
					<div class="form-group">
						{!! Form::label('mobile', 'Mobile NO:', ['class'=>'form-control-label']) !!}
						<!-- <label for="postal-code" class=" form-control-label">Mobile NO: </label> -->
						{!! Form::text('mobile', null, ['placeholder'=>'Enter your mobile number', 'class'=>'form-control']) !!}
						<!-- <input type="text" id="postal-code" placeholder="Enter your mobile number" class="form-control"> -->
						{!! $errors->first('mobile', '<p class="text text-danger">:message</p>') !!}
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('email', 'Contact person Email:', ['class'=>'form-control-label']) !!}
				<!-- <label for="country" class=" form-control-label">Email</label> -->
				{!! Form::text('email', null, ['placeholder'=>'Enter your email address', 'class'=>'form-control']) !!}
				<!-- <input type="text" id="country" placeholder="Enter your email address" class="form-control"> -->
				{!! $errors->first('email', '<p class="text text-danger">:message</p>') !!}
			</div>

		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary btn-lg pull-right">
				<i class="fa fa-dot-circle-o"></i> Submit
			</button>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@endsection