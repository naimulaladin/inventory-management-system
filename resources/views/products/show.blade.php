@extends('layouts.app')
@section('content')
<div class="col-lg-6">
    <!-- TOP CAMPAIGN-->
    <div class="top-campaign">
        <h3 class="title-3 m-b-30">Detail Information</h3>
        <div class="table-responsive">
            <table class="table table-top-campaign">
                <tbody>
                  @if(isset($products))
                    <tr>
                        <td>Product Name: </td>
                        <td class="float-left">{{$products->product_name}}</td>
                    </tr>
                     <tr>
                        <td>Category: </td>
                        <td class="float-left">{{$products->Category->category_name}}</td>
                    </tr>
                    <tr>
                        <td>Product's Descriptions</td>
                        <td  class="float-left">{{$products->description}}</td>
                    </tr>
                    <tr>
                        <td>Image: </td>
                        <td class="float-left">{{$products->image_path}}</td>
                    </tr>
                    @else
                        <tr>
                            <td colspan="4" class="bg-warning">No Data available</td>
                        </tr>
                     @endif
                   
                </tbody>
            </table>
        </div>
    </div>
    <!--  END TOP CAMPAIGN-->
</div>
@endsection