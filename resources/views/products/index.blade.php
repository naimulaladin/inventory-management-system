@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('success_message')}}
            </div>
        @endif
        @if(Session::has('error_message'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{Session::get('error_message')}}
            </div>
        @endif
        <a class="btn btn-primary pull-right" style="margin-bottom: 10px;" href="{{URL::route('products.create')}}">
            ADD NEW
            <i class="fas fa-plus-circle"></i>
        </a>
        <div class="table-responsive m-b-40">

            <table class="table table-borderless table-data3">
                <thead>
                <tr>
                    <th>SL. No</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php($i=1)
                @forelse($products as $product)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$product->product_name}}</td>
                        <td>{{$product->category_id}}</td>
                        <td>{{$product->description}}</td>
                        <td>{{$product->image_path}}</td>
                        <td style="display: flex;;justify-content:space-around;flex-wrap: nowrap;">
                            <a class="btn btn-primary btn-sm" style="margin-right: 2px" href="{{URL::route('products.edit',$product->id)}}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a class="btn btn-info btn-sm" href="{{URL::route('products.show',$product->id)}}">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="bg-warning">No Data available</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection()