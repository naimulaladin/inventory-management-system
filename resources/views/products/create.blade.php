@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        {!! Form::open(['route' => 'products.store', 'files' => true])!!}
        <div class="card" style="overflow: hidden">
            <div class="card-header">
                <strong>Products Entry</strong>
                <small>Form</small>
            </div>
            <div class="card-body card-block">
                <div class="form-group">
                {!! Form::label('product_name','Products Name',['class'=>'form-control-label']) !!}
                <!-- <label for="company" class=" form-control-label">Company Name</label> -->
                {!! Form::text("product_name",null,['placeholder'=>'Enter your product name','class'=>"form-control"])!!}
                <!-- <input type="text" id="company" placeholder="Enter your company name" class="form-control"> -->
                {!! $errors->first('product_name','<p class="text text-danger">:message</p>') !!}
                </div>
                <div class="form-group">
                {!! Form::label('category_id', 'Select Category', ['class'=>'form-control-label']) !!}
                <!-- <label for="ceo_name" class=" form-control-label">CEO Name</label> -->
                {!! Form::select('category_id', $categories, null, ['class'=>'form-control']) !!}
                <!-- <input type="text" id="vat" placeholder="Enter your CEO name" class="form-control"> -->
                {!! $errors->first('category_id', '<p class="text text-danger">:message</p>') !!}
                </div>
                <div class="form-group">
                {!! Form::label('description', 'Product Description', ['class'=>'form-control-label']) !!}
                <!-- <label for="street" class=" form-control-label">Address</label> -->
                {!! Form::text('description', null, ['placeholder'=>'Enter description', 'class'=>'form-control']) !!}
                <!-- <input type="text" id="street" placeholder="Enter your address" class="form-control">
				-->
                {!! $errors->first('description', '<p class="text text-danger">:message</p>') !!}
                </div>
                
                <div class="form-group">
                {!! Form::file('image') !!}
                {!! $errors->first('image_path', '<p class="text text-danger">:message</p>') !!}
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-lg pull-right">
                    <i class="fa fa-dot-circle-o"></i> Submit
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection